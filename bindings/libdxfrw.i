%module libdxfrw
%include "std_string.i"

%{
#include "libdxfrw.h"
#include "libdwgr.h"
#include "drw_entities.h"
#include "drw_interface.h"
#include "drw_objects.h"
%}

%include "libdxfrw.h"
%include "libdwgr.h"
%include "drw_entities.h"
%include "drw_interface.h"
%include "drw_objects.h"
